// To parse this JSON data, do
//
//     final details = detailsFromJson(jsonString);


import 'dart:convert';

List<Details> detailsFromJson(String str) =>
    List<Details>.from(json.decode(str).map((x) => Details.fromJson(x)));

String detailsToJson(List<Details> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Details {
  Details({
    required this.restaurantId,
    required this.restaurantName,
    required this.restaurantImage,
    required this.tableId,
    required this.tableName,
    required this.branchName,
    required this.nexturl,
    required this.tableMenuList,
  });

  String restaurantId;
  String restaurantName;
  String restaurantImage;
  String tableId;
  String tableName;
  String branchName;
  String nexturl;
  List<TableMenuList>? tableMenuList;

  factory Details.fromJson(Map<String, dynamic> json) => Details(
        restaurantId:
            json["restaurant_id"] == null ? null : json["restaurant_id"],
        restaurantName:
            json["restaurant_name"] == null ? null : json["restaurant_name"],
        restaurantImage:
            json["restaurant_image"] == null ? null : json["restaurant_image"],
        tableId: json["table_id"] == null ? null : json["table_id"],
        tableName: json["table_name"] == null ? null : json["table_name"],
        branchName: json["branch_name"] == null ? null : json["branch_name"],
        nexturl: json["nexturl"] == null ? null : json["nexturl"],
        tableMenuList: json["table_menu_list"] == null
            ? null
            : List<TableMenuList>.from(
                json["table_menu_list"].map((x) => TableMenuList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "restaurant_id":restaurantId,
        "restaurant_name": restaurantName,
        "restaurant_image":  restaurantImage,
        "table_id": tableId,
        "table_name":  tableName,
        "branch_name":  branchName,
        "nexturl": nexturl,
        "table_menu_list": tableMenuList == null
            ? null
            : List<dynamic>.from(tableMenuList!.map((x) => x.toJson())),
      };
}

class TableMenuList {
  TableMenuList({
    required this.menuCategory,
    required this.menuCategoryId,
    required this.menuCategoryImage,
    required this.nexturl,
    required this.categoryDishes,
  });

  String menuCategory;
  String menuCategoryId;
  String menuCategoryImage;
  String nexturl;
  List<CategoryDish>? categoryDishes;

  factory TableMenuList.fromJson(Map<String, dynamic> json) => TableMenuList(
        menuCategory:
            json["menu_category"] == null ? null : json["menu_category"],
        menuCategoryId:
            json["menu_category_id"] == null ? null : json["menu_category_id"],
        menuCategoryImage: json["menu_category_image"] == null
            ? null
            : json["menu_category_image"],
        nexturl: json["nexturl"] == null ? null : json["nexturl"],
        categoryDishes: json["category_dishes"] == null
            ? null
            : List<CategoryDish>.from(
                json["category_dishes"].map((x) => CategoryDish.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "menu_category":  menuCategory,
        "menu_category_id": menuCategoryId,
        "menu_category_image":
             menuCategoryImage,
        "nexturl":  nexturl,
        "category_dishes": categoryDishes == null
            ? null
            : List<dynamic>.from(categoryDishes!.map((x) => x.toJson())),
      };
}

class AddonCat {
  AddonCat({
    required this.addonCategory,
    required this.addonCategoryId,
    required this.addonSelection,
    required this.nexturl,
    required this.addons,
  });

  String addonCategory;
  String addonCategoryId;
  var addonSelection;
  String nexturl;
  List<CategoryDish>? addons;

  factory AddonCat.fromJson(Map<String, dynamic> json) => AddonCat(
        addonCategory:
            json["addon_category"] == null ? null : json["addon_category"],
        addonCategoryId: json["addon_category_id"] == null
            ? null
            : json["addon_category_id"],
        addonSelection:
            json["addon_selection"] == null ? null : json["addon_selection"],
        nexturl: json["nexturl"] == null ? null : json["nexturl"],
        addons: json["addons"] == null
            ? null
            : List<CategoryDish>.from(
                json["addons"].map((x) => CategoryDish.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "addon_category": addonCategory,
        "addon_category_id": addonCategoryId,
        "addon_selection": addonSelection == null ? null : addonSelection,
        "nexturl": nexturl,
        "addons": addons == null
            ? null
            : List<dynamic>.from(addons!.map((x) => x.toJson())),
      };
}

class CategoryDish {
  CategoryDish({
      required this.dishId,
     required this.dishName,
    required this.dishPrice,
    required this.dishImage,
    required this.dishCurrency,
    required this.dishCalories,
    required this.dishDescription,
    required this.dishAvailability,
    required this.dishType,
    required this.nexturl,
    required this.addonCat,
   required  this.count
  });

  String dishId;
  String dishName;
  double dishPrice;
  String dishImage;
  DishCurrency? dishCurrency;
  var dishCalories;
  String dishDescription;
  bool dishAvailability;
  var dishType;
  var nexturl;
  var count;
  List<AddonCat>? addonCat;

  factory CategoryDish.fromJson(Map<String, dynamic> json) => CategoryDish(
        dishId: json["dish_id"] == null ? null : json["dish_id"],
        dishName: json["dish_name"] == null ? null : json["dish_name"],
        dishPrice:
            json["dish_price"] == null ? null : json["dish_price"].toDouble(),
        dishImage: json["dish_image"] == null ? null : json["dish_image"],
        dishCurrency: json["dish_currency"] == null
            ? null
            : dishCurrencyValues.map![json["dish_currency"]],
        dishCalories:
            json["dish_calories"] == null ? null : json["dish_calories"],
        dishDescription:
            json["dish_description"] == null ? null : json["dish_description"],
        dishAvailability: json["dish_Availability"] == null
            ? null
            : json["dish_Availability"],
        dishType: json["dish_Type"] == null ? null : json["dish_Type"],
        nexturl: json["nexturl"] == null ? null : json["nexturl"],
           count: json["count"] == null ? null : json["count"],
        addonCat: json["addonCat"] == null
            ? null
            : List<AddonCat>.from(
                json["addonCat"].map((x) => AddonCat.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "dish_id":  dishId,
        "dish_name":  dishName,
        "dish_price":  dishPrice,
        "dish_image": dishImage,
        "dish_currency": dishCurrency == null
            ? null
            : dishCurrencyValues.reverse![dishCurrency],
        "dish_calories": dishCalories == null ? null : dishCalories,
        "dish_description":  dishDescription,
        "dish_Availability":  dishAvailability,
        "dish_Type": dishType == null ? null : dishType,
        "nexturl": nexturl == null ? null : nexturl,
            "count": count == null ? null : count,
        "addonCat": addonCat == null
            ? null
            : List<dynamic>.from(addonCat!.map((x) => x.toJson())),
      };
}

enum DishCurrency { SAR }

final dishCurrencyValues = EnumValues({"SAR": DishCurrency.SAR});

class EnumValues<T> {
  Map<String, T>? map;
  Map<T, String>? reverseMap;

  EnumValues(this.map);

  Map<T, String>? get reverse {
    if (reverseMap == null) {
      reverseMap = map!.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
