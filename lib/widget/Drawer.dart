import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';


import 'Exit.dart';

class NavDrawer extends StatelessWidget {


final FirebaseAuth auth = FirebaseAuth.instance;
final FirebaseAuth firebaseAuth = FirebaseAuth.instance;






  @override
  Widget build(BuildContext context) {
     final User ? user = auth.currentUser;
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
      

 Container(
  decoration: BoxDecoration(
    gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            stops: [0.0, 1.0],
            colors: [
              Colors.green.shade400,
              Colors.green.shade700,
            ],
          ) ,
        borderRadius: BorderRadius.only(
       
            bottomRight: Radius.circular(20.0),
          
            bottomLeft: Radius.circular(20.0)),
      ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SafeArea(
                  child: Container(
                    width: 100,
                    height: 150,
                    child: Image(image: AssetImage("assets/firebase_logo.png"),)),
                ),

                  Text(user!.phoneNumber .toString(),style: TextStyle(fontSize: 18),),
                  SizedBox(height: 10,),
              Text(user.email .toString(),style: TextStyle(fontSize: 18),),
                SizedBox(height: 10,),
               // Text("Id:410")

                
              ],
            ),
          ),
        ),

          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Logout'),
            onTap: () => {exitApp(context)},
          ),
        ],
      ),
    );
  }
}