import 'package:flutter/material.dart';
import 'package:machinetest/Controller/Cartmodelcontroller.dart';
import 'package:provider/provider.dart';
class Summarywidget extends StatelessWidget {


  const Summarywidget({Key? key}) : super(key: key);
  

  @override
  Widget build(BuildContext context) {
        var cart = context.watch<CartModel>();
    return Flexible(
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: cart.getData.length,
          itemBuilder: (context, i) {
            return Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          width: 20,
                          height: 20,
                          child: Image.asset("assets/nonveg.png")),
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              cart.getData.elementAt(i).name.toString(),
                              style: TextStyle(
                                  color: Colors.grey[700],
                                  fontWeight: FontWeight.w500,
                                  fontSize: 15),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "INR" + cart.getData.elementAt(i).rate.toString(),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              cart.getData.elementAt(i).calories.toString(),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: 120,
                        padding: EdgeInsets.all(3),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.green),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            InkWell(
                                onTap: () {
                                  cart.removeCardManager(
                                      cart.getData.elementAt(i).name,
                                      cart.getData.elementAt(i).id,
                                      cart.getData.elementAt(i).rate,
                                      cart.getData.elementAt(i).calories,
                                      1);
                                },
                                child: Icon(
                                  Icons.remove,
                                  color: Colors.white,
                                  size: 18,
                                )),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 3),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 3, vertical: 2),
                              child: Text(
                                cart.quanity(cart.getData.elementAt(i).id),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18),
                              ),
                            ),
                            InkWell(
                                onTap: () {
                                  cart.addCardManager(
                                      cart.getData.elementAt(i).name,
                                      cart.getData.elementAt(i).id,
                                      cart.getData.elementAt(i).rate,
                                      cart.getData.elementAt(i).calories,
                                      1);
                                },
                                child: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                  size: 18,
                                )),
                          ],
                        ),
                      ),
                      Container(
                          width: 100,
                          height: 50,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("INR"),
                              Text(cart.getData.elementAt(i).total.toString()),
                            ],
                          )),
                    ],
                  ),
                ),
                Divider(
                  thickness: 1,
                )
              ],
            );
          }),
    );
  }
}
