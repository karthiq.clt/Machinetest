import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:machinetest/screens/Loginscreen.dart';

exitApp(BuildContext context) {
  return showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Text('Do you want to exit this application?'),
      content: Text('We hate to see you leave...'),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.of(context).pop(false);
          },
          child: Text('No'),
        ),
        TextButton(
          onPressed: () async {
            close(context);

            //   SystemChannels.platform.invokeMethod('SystemNavigator.pop');
          },
          child: Text('Yes'),
        ),
      ],
    ),
  );
}

void close(BuildContext context) async {
  try {
    await FirebaseAuth.instance.signOut();
 Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
    Loginscreen()), (Route<dynamic> route) => false);
  } catch (e) {
    // showErrorMessage(e);

    print(e);
    //  Navigator.pop(context);
  }
}
