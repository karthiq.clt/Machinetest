
import 'package:flutter/material.dart';
import 'package:machinetest/Controller/Cartmodelcontroller.dart';
import 'package:machinetest/Model/Details.dart';
import 'package:provider/provider.dart';
class Listitemwidget extends StatefulWidget {
  final List<CategoryDish>? data;
  const Listitemwidget({
    this.data,
    Key? key,
  }) : super(key: key);

  @override
  _ListitemwidgetState createState() => _ListitemwidgetState();
}

class _ListitemwidgetState extends State<Listitemwidget> {


  @override
  Widget build(BuildContext context) {
    var cart = context.watch<CartModel>();

    return ListView.builder(
      itemCount: widget.data!.length,
      itemBuilder: (context, i) {
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      width: 20,
                      height: 20,
                      child: Image.asset("assets/nonveg.png")),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.data!.elementAt(i).dishName.toString(),
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w600,
                              fontSize: 15),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Rs " +
                                  widget.data!
                                      .elementAt(i)
                                      .dishPrice
                                      .toString(),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 13),
                            ),
                            Text(
                              "Calories " +
                                  widget.data!
                                      .elementAt(i)
                                      .dishCalories
                                      .toString(),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 13),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(widget.data!
                            .elementAt(i)
                            .dishDescription
                            .toString()),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          width: 120,
                          padding: EdgeInsets.all(3),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.green),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              InkWell(
                                  onTap: () {
                                    cart.addCardManager(
                                        widget.data!.elementAt(i).dishName,
                                        widget.data!.elementAt(i).dishId,
                                        widget.data!.elementAt(i).dishPrice,
                                        widget.data!
                                            .elementAt(i)
                                            .dishCalories
                                            .toString(),
                                        1);
                                  },
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.white,
                                    size: 20,
                                  )),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 3),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 3, vertical: 2),
                                child: Text(
                                  cart.quanity(
                                      widget.data!.elementAt(i).dishId),
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18),
                                ),
                              ),
                              InkWell(
                                  onTap: () {
                                    cart.removeCardManager(
                                        widget.data!.elementAt(i).dishName,
                                        widget.data!.elementAt(i).dishId,
                                        widget.data!.elementAt(i).dishPrice,
                                        widget.data!
                                            .elementAt(i)
                                            .dishCalories
                                            .toString(),
                                        1);
                                  },
                                  child: Icon(
                                    Icons.remove,
                                    color: Colors.white,
                                    size: 20,
                                  )),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Visibility(
                          visible:
                              widget.data!.elementAt(i).addonCat!.length == 0
                                  ? false
                                  : true,
                          child: Text(
                            "Customizations Availible",
                            style: TextStyle(color: Colors.red),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                  Container(
                      width: 100,
                      height: 50,
                      child:
                          Image.network(widget.data!.elementAt(i).dishImage)),
                ],
              ),
            ),
            Divider(
              thickness: 1,
            )
          ],
        );
      },
    );
  }
}
