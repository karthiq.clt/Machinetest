import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:machinetest/Controller/Signin.dart';
import 'package:machinetest/screens/Otpscreen.dart';
import 'package:machinetest/screens/Showitem.dart';

class Loginscreen extends StatefulWidget {
  const Loginscreen({Key? key}) : super(key: key);

  @override
  _LoginscreenState createState() => _LoginscreenState();
}

class _LoginscreenState extends State<Loginscreen> {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<bool> _getUesr() async {
    if (FirebaseAuth.instance.currentUser?.uid != null) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: _getUesr(),
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (snapshot.hasData) {
          if (!snapshot.data!) {
            return Scaffold(
              body: Column(
                children: [
                  Expanded(
                      flex: 2,
                      child: ConstrainedBox(
                        constraints: const BoxConstraints(
                          maxWidth: 90,
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.scaleDown,
                              image: AssetImage("assets/firebase_logo.png"),
                            ),
                          ),
                        ),
                      )),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 35.0, right: 35),
                          child: TextButton(
                            style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.blue),
                              shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(40),
                                ),
                              ),
                            ),
                            onPressed: () async {
                              bool value = await Signin.signInWithGoogle(_auth);

                              if (!value) {
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(SnackBar(
                                  content: Text("Failed to sign in"),
                                ));
                              } else {
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            Showitem()));
                              }

                         
                            },
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(00, 10, 0, 10),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Image(
                                        image: AssetImage(
                                            "assets/google_logo.png"),
                                        height: 25.0),
                                  ),
                                  Expanded(
                                      child: Center(
                                    child: Padding(
                                        padding:
                                            const EdgeInsets.only(right: 35),
                                        child: Text('Google',
                                            style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.white))),
                                  ))
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 35.0, right: 35, top: 20),
                          child: Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                stops: [0.0, 1.0],
                                colors: [
                                  Colors.green.shade400,
                                  Colors.green.shade700,
                                ],
                              ),
                              color: Colors.deepPurple.shade300,
                              borderRadius: BorderRadius.circular(40),
                            ),
                            child: TextButton(
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                    Colors.transparent),
                                shadowColor: MaterialStateProperty.all(
                                    Colors.transparent),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(40),
                                  ),
                                ),
                              ),
                              onPressed: () async {
                              Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                         Otpscreen   ()));
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(00, 10, 0, 10),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(left: 10.0),
                                      child: Image(
                                          image: AssetImage(
                                              "assets/Phoneicon.png"),
                                          height: 25.0),
                                    ),
                                    Expanded(
                                        child: Center(
                                      child: Padding(
                                          padding:
                                              const EdgeInsets.only(right: 35),
                                          child: Text('Phone',
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  color: Colors.white))),
                                    ))
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            );
          }

          if (snapshot.data!) {
            return Showitem();
          }
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}
