import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:machinetest/screens/Showitem.dart';

class Otpscreen extends StatefulWidget {
  const Otpscreen({Key? key}) : super(key: key);

  @override
  _OtpscreenState createState() => _OtpscreenState();
}

class _OtpscreenState extends State<Otpscreen> {
  TextEditingController _numbercontroller = new TextEditingController();
  TextEditingController _otpcontroller = new TextEditingController();

  String _verificationId = '';
  FirebaseAuth _auth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.only(left: 20.0, right: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              controller: _numbercontroller,
              decoration: new InputDecoration(
                  labelText: "Enter your number without country code"),
              keyboardType: TextInputType.number,
            ),
            TextButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.red)),
                onPressed: () {
                  _verifyPhoneNumber();
                },
                child: Text("Submit")),
            TextField(
                decoration: new InputDecoration(labelText: "Otp"),
                keyboardType: TextInputType.number,
                controller: _otpcontroller),
            TextButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.red)),
                onPressed: () {
                  _signInWithPhoneNumber();
                },
                child: Text("Submit")),
          ],
        ),
      ),
    );
  }

  Future<void> _verifyPhoneNumber() async {
    // setState(() {
    //   _message = '';
    // });

    PhoneVerificationCompleted verificationCompleted =
        (PhoneAuthCredential phoneAuthCredential) async {
      await _auth.signInWithCredential(phoneAuthCredential);
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (BuildContext context) => Showitem()));

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
            'Phone number automatically verified and user signed in: $phoneAuthCredential'),
      ));
    };

    PhoneVerificationFailed verificationFailed =
        (FirebaseAuthException authException) {
      // setState(() {
      //   _message =
      //       'Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}';
      // });
    };

    PhoneCodeSent codeSent =
        (String verificationId, [int? forceResendingToken]) async {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Please check your phone for the verification code.'),
      ));

      _verificationId = verificationId;
    };

    PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      _verificationId = verificationId;
    };

    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: "+91" + _numbercontroller.text.toString(),
          timeout: const Duration(seconds: 5),
          verificationCompleted: verificationCompleted,
          verificationFailed: verificationFailed,
          codeSent: codeSent,
          codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Failed to Verify Phone Number: $e'),
      ));
    }
  }

  Future<void> _signInWithPhoneNumber() async {
    try {
      final PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: _verificationId,
        smsCode: _otpcontroller.text.toString(),
      );
      final User? user = (await _auth.signInWithCredential(credential)).user;

      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (BuildContext context) => Showitem()));
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Successfully signed in UID: ${user!.uid}'),
      ));
    } catch (e) {
      print(e);

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Failed to sign in'),
      ));
    }
  }
}
