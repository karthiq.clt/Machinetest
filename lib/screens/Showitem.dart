import 'package:flutter/material.dart';
import 'package:machinetest/Controller/Cartmodelcontroller.dart';
import 'package:machinetest/Model/Details.dart';
import 'package:machinetest/Service/Apicall.dart';
import 'package:machinetest/screens/Ordersummery.dart';
import 'package:machinetest/widget/Drawer.dart';
import 'package:provider/provider.dart';

class Showitem extends StatefulWidget {
  const Showitem({Key? key}) : super(key: key);

  @override
  _ShowitemState createState() => _ShowitemState();
}

class _ShowitemState extends State<Showitem> {
  @override
  Widget build(BuildContext context) {
    //   var cart = context.watch<CartModel>();
    return Scaffold(
    // appBar: new AppBar(

    // actions: <Widget>[

    //   new Padding(padding: const EdgeInsets.all(10.0),

    //     child: new Container(
    //       height: 150.0,
    //       width: 30.0,
    //       child: new GestureDetector(
    //         onTap: () {
    //           Navigator.of(context).push(
    //               new MaterialPageRoute(
    //                   builder:(BuildContext context) =>
    //                   new Ordersummery()
    //               )
    //           );
    //         },

    //         child: new Stack(

    //           children: <Widget>[
    //             new IconButton(icon: new Icon(Icons.shopping_cart,
    //               color: Colors.white,),
    //                 onPressed: null,
    //             ),
    //                  cart.newDataCount ==0 ? new Container() :
    //             new Positioned(

    //                 child: new Stack(
    //                   children: <Widget>[
    //                     new Icon(
    //                         Icons.brightness_1,
    //                         size: 20.0, color: Colors.green[800]),
    //                     new Positioned(
    //                         top: 3.0,
    //                         right: 4.0,
    //                         child: new Center(
    //                           child: new Text(
    //                        cart.newDataCount.toString(),
    //                             style: new TextStyle(
    //                                 color: Colors.white,
    //                                 fontSize: 11.0,
    //                                 fontWeight: FontWeight.w500
    //                             ),
    //                           ),
    //                         )),


    //                   ],
    //                 )),

    //           ],
    //         ),
    //       )
    //     )

    //     ,)],),
        body: FutureBuilder<List<Details>?>(
          future: callapi(), // async work
          builder:
              (BuildContext context, AsyncSnapshot<List<Details>?> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return Center(child: CircularProgressIndicator());
              default:
                if (snapshot.hasError)
                  return Text('Error: ${snapshot.error}');
                else if (snapshot.hasData) {
                  var cart = context.watch<CartModel>();
                  List<Tab> tabs = [];

                  for (int i = 0;
                      i < snapshot.data!.elementAt(0).tableMenuList!.length;
                      i++) {
                    tabs.add(Tab(
                      child: Text(
                        snapshot.data!
                            .elementAt(0)
                            .tableMenuList!
                            .elementAt(i)
                            .menuCategory,
                        style: TextStyle(color: Colors.black),
                      ),
                    ));
                  }
                  return Scaffold(
                     drawer: NavDrawer(),
appBar: new AppBar(backgroundColor: Colors.white,     leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(Icons.menu_rounded,color: Colors.grey[800]),
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),

    actions: <Widget>[
      

      new Padding(padding: const EdgeInsets.all(10.0),

        child: new Container(
          height: 150.0,
          width: 30.0,
          child: new GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                  new MaterialPageRoute(
                      builder:(BuildContext context) =>
                      new Ordersummery()
                  )
              );
            },

            child: new Stack(

              children: <Widget>[
                new IconButton(icon: new Icon(Icons.shopping_cart,
                  color: Colors.grey[600],),
                    onPressed: null,
                ),
                     cart.newDataCount ==0 ? new Container() :
                new Positioned(

                    child: new Stack(
                      children: <Widget>[
                        new Icon(
                            Icons.brightness_1,
                            size: 20.0, color: Colors.red[800]),
                        new Positioned(
                            top: 3.0,
                            right: 6.0,
                            child: new Center(
                              child: new Text(
                           cart.newDataCount.toString(),
                                style: new TextStyle(
                                    color: Colors.white,
                                    fontSize: 11.0,
                                    fontWeight: FontWeight.w500
                                ),
                              ),
                            )),


                      ],
                    )),

              ],
            ),
          )
        )

        ,)],),

                    body: DefaultTabController(
                      length: snapshot.data!.elementAt(0).tableMenuList!.length,
                      child: Column(
                        children: [
                          Container(
                            height: 50,
                            child: TabBar(
                              isScrollable: true,
                              tabs: tabs,
                            ),
                          ),
                  
                          Expanded(
                            child: TabBarView(
                              children: List<Widget>.generate(
                                  snapshot.data!
                                      .elementAt(0)
                                      .tableMenuList!
                                      .length, (int index) {
                                print(index);
                  
                                return Listitemwidget(
                                    data: snapshot.data!
                                        .elementAt(0)
                                        .tableMenuList!
                                        .elementAt(index)
                                        .categoryDishes);
                              }),
                            ),
                          )
                  
                          //    Expanded(child: Listitemwidget(data: snapshot.data)),
                        ],
                      ),
                    ),
                  );
                }
                return Container();
            }
          },
        ));
  }

  Future<List<Details>?> callapi() async {
    return await Apicalls.fetchAlbum();
  }
}

class Listitemwidget extends StatefulWidget {
  final List<CategoryDish>? data;
  //final List<Details>? data;
  const Listitemwidget({
    this.data,
    Key? key,
  }) : super(key: key);

  @override
  _ListitemwidgetState createState() => _ListitemwidgetState();
}

class _ListitemwidgetState extends State<Listitemwidget> {


  @override
  Widget build(BuildContext context) {
    var cart = context.watch<CartModel>();

    return ListView.builder(
      itemCount: widget.data!.length,
      itemBuilder: (context, i) {
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      width: 20,
                      height: 20,
                      child: Image.asset("assets/nonveg.png")),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.data!.elementAt(i).dishName.toString(),
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w600,
                              fontSize: 15),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Rs " +
                                  widget.data!
                                      .elementAt(i)
                                      .dishPrice
                                      .toString(),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 13),
                            ),
                            Text(
                              "Calories " +
                                  widget.data!
                                      .elementAt(i)
                                      .dishCalories
                                      .toString(),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 13),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(widget.data!
                            .elementAt(i)
                            .dishDescription
                            .toString()),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          width: 120,
                          padding: EdgeInsets.all(3),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.green),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              InkWell(
                                  onTap: () {
                                    cart.addCardManager(
                                      widget.data!.elementAt(i).dishName ,widget.data!.elementAt(i).dishId,
                        widget.data!.elementAt(i).dishPrice,
                                        widget.data!.elementAt(i).dishCalories.toString(),
                                     1);
                                  },
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.white,
                                    size: 20,
                                  )),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 3),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 3, vertical: 2),
                                child: Text(
                                 cart.quanity(widget.data!.elementAt(i).dishId),
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18),
                                ),
                              ),
                              InkWell(
                                  onTap: () {

      cart.removeCardManager(
                                      widget.data!.elementAt(i).dishName ,widget.data!.elementAt(i).dishId,
                                        widget.data!.elementAt(i).dishPrice,
                                        widget.data!.elementAt(i).dishCalories.toString(),
                                     1);

                                  },
                                  child: Icon(
                                    Icons.remove,
                                    color: Colors.white,
                                    size: 20,
                                  )),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Visibility(
                          visible:
                              widget.data!.elementAt(i).addonCat!.length == 0
                                  ? false
                                  : true,
                          child: Text(
                            "Customizations Availible",
                            style: TextStyle(color: Colors.red),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                  Container(
                      width: 100,
                      height: 50,
                      child:
                          Image.network(widget.data!.elementAt(i).dishImage)),
                ],
              ),
            ),
            Divider(
              thickness: 1,
            )
          ],
        );
      },
    );
  }
}
