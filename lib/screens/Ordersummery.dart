import 'package:flutter/material.dart';
import 'package:machinetest/Controller/Cartmodelcontroller.dart';
import 'package:machinetest/widget/Summarywidget.dart';
import 'package:provider/provider.dart';

class Ordersummery extends StatefulWidget {
  const Ordersummery({Key? key}) : super(key: key);

  @override
  _OrdersummeryState createState() => _OrdersummeryState();
}

class _OrdersummeryState extends State<Ordersummery> {
  @override
  Widget build(BuildContext context) {
    var cart = context.watch<CartModel>();
    return Scaffold(
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ElevatedButton(
          onPressed: () {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text('Order placed Successfully'),
            ));

            cart.clearCardManger();
            Navigator.pop(context);
          },
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ))),
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text("Place Order",style: TextStyle(fontSize: 15),),
          ),
        ),
      ),
      appBar: AppBar(
        iconTheme: IconThemeData(
    color: Colors.black
  ),
        backgroundColor: Colors.white
        ,
        title: Text(
          "Order summary",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
  
            children: [
              Container(
                height: 50,
                decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: Center(
                    child: Text(
                  cart.newDataCount.toString() + " Dishes",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w500),
                )),
              ),
              SizedBox(
                height: 20,
              ),
            Summarywidget(),
              Padding(
                padding: const EdgeInsets.only(
                    top: 10, bottom: 25, left: 10, right: 10),
                child: Row(
                  children: [
                    Text("Total Amount",style: TextStyle(color: Colors.green,fontSize: 15),),
                    Spacer(),
                    Text(cart.getTotal.toString())
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
