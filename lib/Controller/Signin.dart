import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class Signin {
  static Future<bool> signInWithGoogle(FirebaseAuth _auth) async {
    try {
      UserCredential userCredential;

      {
        final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();
        final GoogleSignInAuthentication googleAuth =
            await googleUser!.authentication;
        final googleAuthCredential = GoogleAuthProvider.credential(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );
        userCredential = await _auth.signInWithCredential(googleAuthCredential);
      }

     // final user = userCredential.user;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
