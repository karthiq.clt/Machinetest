import 'dart:collection';
import 'package:flutter/material.dart';

class CartModel extends ChangeNotifier {
 List<Cart> _addNewData = [];
 int get newDataCount {
    return _addNewData.length;
  }

  List<Cart> get getData {
    return _addNewData;
  }

  double get getTotal {
    double _total = 0;
    for (int itemcount = 0; itemcount < newDataCount; itemcount++) {
      _total = _total + _addNewData[itemcount].total;
    }                         
    return _total;
  }

  UnmodifiableListView<Cart> get tasks {
    return UnmodifiableListView(_addNewData);
  }

  String quanity(String? id) {
    String qunity = "0";
    for (int itemcount = 0; itemcount < newDataCount; itemcount++) {
      if (_addNewData[itemcount].id == id) {
        qunity = _addNewData[itemcount].quantity.toString();
        break;
      }
    }
    return qunity;
  }



  void addCardManager(
    String itemName,
    String id,
    double itemRate,
    String calories,
    int quantity,
  ) {
    final newAddItem = Cart(
        name: itemName,
        rate: itemRate,
        id: id,
        calories: calories,
        total: itemRate);
    if (_addNewData.length != 0) {
      bool isFound = false;
      for (int itemcount = 0; itemcount < newDataCount; itemcount++) {
        if (_addNewData[itemcount].name == newAddItem.name) {
          print("addCard");
          isFound = true;
          _addNewData[itemcount].toggleDone();
          notifyListeners();
          break;
        }
      }
      if (!isFound) {
        _addNewData.add(newAddItem);
        notifyListeners();
      }
    } else {
      _addNewData.add(newAddItem);

      notifyListeners();
    }
  }

  void clearCardManger() {
    _addNewData.clear();
    notifyListeners();
  }

  void removeCardManager(String itemName, String id, double itemRate,
      String calories, int quantity) {
    final newAddItem = Cart(
      name: itemName,
      rate: itemRate,
      calories: calories,
      id: id,
    );
    print(_addNewData);
    if (_addNewData.length != 0) {
     // bool isFound = false;

      for (int itemcount = 0; itemcount < newDataCount; itemcount++) {
        if (_addNewData[itemcount].name == newAddItem.name) {
          print("RemoveCard");
    //      isFound = true;
          decrease(_addNewData[itemcount]);
          notifyListeners();
          break;
        }
      }
    }
  }

  void getCardManager(
    String itemName,
    String id,
    double itemRate,
    String calories,
    int quantity,
  ) {
    final newAddItem =
        Cart(name: itemName, rate: itemRate, id: id, calories: calories);
    print(_addNewData);
    if (_addNewData.length != 0) {
     // bool isFound = false;

      for (int itemcount = 0; itemcount < newDataCount; itemcount++) {
        if (_addNewData[itemcount].name == newAddItem.name) {
          print("RemoveCard");
       //   isFound = true;
          decrease(_addNewData[itemcount]);
          notifyListeners();
          break;
        }
      }
    }
  }

  void updateTask(Cart task) {
    task.toggleDone();
    notifyListeners();
  }

  void decrease(Cart task) {
    if (task.quantity == 1) {
      removeCard(task);
    }
    task.decreaseDown();
    notifyListeners();
  }

  void removeCard(Cart task) {
    _addNewData.remove(task);
    notifyListeners();
  }
}

class Cart {
  final String name;
  final String id;
  final String calories;
  final double rate;
  double total;
  int quantity;

  Cart({
    required this.name,
    required this.id,
    required this.rate,
    required this.calories,
    this.total = 0,
    this.quantity = 1,
  });

  void toggleDone() {
    quantity++;
    total = quantity * rate;
  }

  void decreaseDown() {
    // ignore: unnecessary_statements
    quantity == 0 ? 0 : quantity--;
    total = quantity * rate;
  }
}
